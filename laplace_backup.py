import pandas as pd
from scipy.stats import laplace

def mediaSalario(dataset):
    datasetTamanho = len(dataset)
    soma = 0

    for i,tupla in dataset.iterrows():
        soma += float(tupla["income"])

    return soma/datasetTamanho

def busca1(dataset, cidade):
    count = 0
    for i,tupla in dataset.iterrows():
        if( tupla["city"] == cidade and float(tupla["income"] ) > 8000 ):
            count += 1

    return count

def busca2(dataset, estado):
    mediaSalarioDataset = mediaSalario(dataset)

    count = 0

    for i,tupla in dataset.iterrows():
        if( tupla["state"] == estado and float(tupla["income"]) > mediaSalarioDataset ):
            count += 1

    return count

def busca3(dataset, estado):
    soma = 0
    tam = 0

    for i,tupla in dataset.iterrows():
        if(tupla["state"] == estado):
            soma += float(tupla["income"])
            tam += 1

    return soma/tam

def sensibilidade(dataset, busca, cidade, estado):
    vals = []

    if(busca == 1):
        originalCount = busca1(dataset, cidade)

        for i,tupla in dataset.iterrows():
            d = dataset.drop([i])
            count = busca1(d, cidade)
            vals.append(abs(count-originalCount))
    elif(busca == 2):
        originalCount = busca2(dataset, estado)

        for i,tupla in dataset.iterrows():
            d = dataset.drop([i])
            count = busca2(d, estado)
            vals.append(abs(count-originalCount))
    elif(busca == 3):
        originalMean = busca3(dataset, estado)

        for i,tupla in dataset.iterrows():
            d = dataset.drop([i])
            mean = busca3(d, estado)
            vals.append(abs(mean-originalMean))

    return max(vals)

def anonimizar(dataset, busca, e, cidade, estado):
    if(busca == 1):
        original = busca1(dataset, cidade)
    elif(busca == 2):
        original = busca2(dataset, estado)
    elif(busca == 3):
        original = busca3(dataset, estado)

    s = sensibilidade(dataset, busca, cidade, estado)
    ruido = laplace.rvs(0,s/e)

    anon = ruido + original
    if(anon < 0):
        anon = 0

    return anon

def consultar(dataset, busca, e, n, cidade, estado):
    for i in range(n):
        anon = anonimizar(dataset, busca, e, cidade, estado)
        # gerar histograma

dataset = pd.read_csv("income.csv")

busca = int(input("Entre qual busca voce deseja fazer - 1, 2 ou 3: "))
e = float(input("Entre o e - 0.01, 0.1 ou 1: "))
n = int(input("Entre o n - 10, 50 ou 100: "))

cidade = ""
estado = ""
if(busca == 1):
    cidade = raw_input("Entre a cidade: ")
else:
    estado = raw_input("Entre o estado: ")

consultar(dataset, busca, e, n, cidade, estado)