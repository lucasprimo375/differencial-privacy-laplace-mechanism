import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import laplace

def mediaSalario(dataset):
    datasetTamanho = len(dataset)
    soma = 0

    for i,tupla in dataset.iterrows():
        soma += float(tupla["income"])

    return soma/datasetTamanho

def busca1(dataset, cidade):
    count = 0
    for i,tupla in dataset.iterrows():
        if( tupla["city"] == cidade and float(tupla["income"] ) > 8000 ):
            count += 1

    return count

def busca2(dataset, estado):
    mediaSalarioDataset = mediaSalario(dataset)
    count = 0

    for i,tupla in dataset.iterrows():
        if( tupla["state"] == estado and float(tupla["income"]) > mediaSalarioDataset ):
            count += 1   
    return count

def busca3(dataset, estado):
    soma = 0
    tam = 0

    for i,tupla in dataset.iterrows():
        if(tupla["state"] == estado):
            soma += float(tupla["income"])
            tam += 1

    return soma/tam

def sensibilidade(dataset, busca, cidade, estado):
    vals = []

    if(busca == 1):
        originalCount = busca1(dataset, cidade)

        for i,tupla in dataset.iterrows():
            d = dataset.drop([i])
            count = busca1(d, cidade)
            vals.append(abs(count-originalCount))
    elif(busca == 2):
        originalCount = busca2(dataset, estado)

        for i,tupla in dataset.iterrows():
            d = dataset.drop([i])
            count = busca2(d, estado)
            vals.append(abs(count-originalCount))
    elif(busca == 3):
        originalMean = busca3(dataset, estado)

        for i,tupla in dataset.iterrows():
            d = dataset.drop([i])
            mean = busca3(d, estado)
            vals.append(abs(mean-originalMean))

    return max(vals)

def anonimizar(dataset, busca, e, cidade, estado):
    if(busca == 1):
        original = busca1(dataset, cidade)
    elif(busca == 2):
        original = busca2(dataset, estado)
    elif(busca == 3):
        original = busca3(dataset, estado)

    s = sensibilidade(dataset, busca, cidade, estado)
    ruido = laplace.rvs(0,s/e)

    anon = ruido + original
    if(anon < 0):
        anon = 0

    return anon

def execucoes(df, Q, e, n, cidade, estado):	
	df_list = []
	for i in range(0, n):
		df_list.append(anonimizar(df, Q, e, cidade, estado))
	df_final = pd.DataFrame(df_list, columns = ['{}{}, consulta Q{} e e={}'.format(cidade,estado, Q, e)])

	return df_final

# e_list = [0.01, 0.1, 1]
# n_list = [10,50,100]

dataset = pd.read_csv("income.csv")[:50]

# execucao: descomentar a forma de execucao (consulta isolada ou histograma de um atributo)

#------------------
# Consulta isolada
#------------------

print("Consultas disponiveis: \n- 1: Para cada cidade, o numero de pessoas (count) que recebem mais do que 8000.\n- 2: Para cada estado, o numero de pessoas (count) que recebem mais do que o valor medio de todas as pessoas no data set. \n- 3: Para cada estado, a media dos salarios.")

Q = int(input("Entre qual busca voce deseja fazer - 1, 2 ou 3: "))
e = float(input("Entre o e - 0.01, 0.1 ou 1: "))

cidade = ""
estado = ""
if(Q == 1):
    cidade = input("Entre a cidade: ")
else:
    estado = input("Entre o estado: ")

print(anonimizar(dataset, Q, e, cidade, estado))

#-------------------
# Histogramas
#-------------------

# CONSULTA: dataset, Q (busca), e, numero de execucoes, cidade, estado

# CONSULTA 1
'''
f = 0

df = execucoes(dataset, 1, 0.01, 10, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 0.01, 50, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 0.01, 100, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 0.1, 10, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 0.1, 50, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 0.1, 100, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 1, 10, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 1, 50, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 1, 1, 100, 'San Francisco', '')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

# CONSULTA 2

df = execucoes(dataset, 2, 0.01, 10, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 0.01, 50, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 0.01, 100, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 0.1, 10, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 0.1, 50, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 0.1, 100, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 1, 10, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 1, 50, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 2, 1, 100, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f+1))
# CONSULTA 3

df = execucoes(dataset, 3, 0.01, 10, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 3, 0.01, 50, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 3, 0.01, 100, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 3, 0.1, 10, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 3, 0.1, 50, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 3, 0.1, 100, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f+1))

df = execucoes(dataset, 3, 1, 10, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 3, 1, 50, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))

df = execucoes(dataset, 3, 1, 100, '', 'CA')
df.plot.hist(grid=True)
f += 1
plt.savefig('Histograma {}.png'.format(f))
'''
#plt.show() #descomentar para abrir janela com plot

